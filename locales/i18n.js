import ReactNative from 'react-native';
import I18n from 'react-native-i18n';

// Import all locales
import en from './en.json';
import cs from './cs.json';
import ru from './ru.json';
import pl from './pl.json';
import sk from './sk.json';

// Should the app fallback to English if user locale doesn't exists
I18n.fallbacks = true;

// Define the supported translations
I18n.translations = {
  en, cs, ru, pl, sk
};

const currentLocale = I18n.currentLocale();

// The method we'll use instead of a regular string
export function strings(name, params = {}) {
  return I18n.t(name, params);
};

export const localization = currentLocale;

export default I18n;
