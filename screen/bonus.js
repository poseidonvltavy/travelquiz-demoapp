import React, {useState, Component} from 'react';
import { View, Text, Image, Dimensions, Alert, TouchableOpacity, ScrollView, ImageBackground } from 'react-native';

/* strings */
import { strings } from '../locales/i18n';

import {background} from '../framework/functions.js';

import AsyncStorage from '@react-native-community/async-storage';
import { useFocusEffect, useIsFocused } from '@react-navigation/native';

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faClock, faEye, faBan, faAd } from '@fortawesome/free-solid-svg-icons'

import { RewardedAd, TestIds, RewardedAdEventType } from '@react-native-firebase/admob';

import { addBonus } from '../framework/functions.js';


var s = require('../framework/styles');

export function BonusScreen({route, navigation}) {
  const isFocused = useIsFocused();
  const [loaded, setLoaded] = useState( false );

  const [bonusVar, setBonusVar] = useState( global.BONUS );
  const [refreshNum, setRefreshNum] = useState( 1 );

  if( __DEV__ ){
    var adUnitId = TestIds.REWARDED;
  } else {
    var adUnitId = 'ca-app-pub-2436333425882202/7749900536';
  }

  const rewarded = RewardedAd.createForAdRequest(adUnitId, {
      requestNonPersonalizedAdsOnly: true,
  });


  const runAd = ( typeReward ) => {
    if( loaded == true ){ return false; }
    setLoaded(true);

    rewarded.onAdEvent((type, error, reward) => {

      if( type == 'error' ){
        Alert.alert( strings('bonus.sorry'), strings('bonus.sorry_text') );
      } else {

        if (type === RewardedAdEventType.LOADED) {
          rewarded.show();
        }
        if (type === RewardedAdEventType.EARNED_REWARD) {
          addBonus( typeReward );
          setBonusVar( global.BONUS );
          setRefreshNum( (refreshNum+1) );
        }

      }

      setLoaded(false);
    });

    rewarded.load();
  }

  useFocusEffect( () => {
    global.CurrentScreen = route.name
  });

  return(
    <View style={[s.container]}>
      <ImageBackground source={background} style={s.backgroundImage}>

      <View style={ ( !loaded ? { transform: [{ scale: 0 }], height: 0, position: 'absolute' } : [s.splashLoading] )}>
        <Text style={[s.splashLoadingText]}>{strings('global.loading')}</Text>
      </View>

      <ScrollView
        refreshNum={ refreshNum }
        >
        <View style={[s.grid]}>

          <TouchableOpacity
            style={ ( bonusVar.scratch !== undefined ? [{ transform: [{ scale: 0 }], height: 0, position: 'absolute' }] : [s.gridItem, { backgroundColor: '#d40d27', borderTopLeftRadius: 0, alignItems: 'flex-start', padding: 20, position: 'relative', marginTop: 35 }] ) }
            onPress={() => { runAd( 'scratch' ); }}>

            <View style={[s.iconBonusHold, {backgroundColor: '#d40d27'}]}>
              <FontAwesomeIcon
                icon={ faEye }
                color={'white'}
                size={ 30 } />
            </View>

            <Text style={[s.title, {textAlign: 'left'}]}>{strings('bonus.scratch_title')}</Text>
            <Text style={{color: 'white'}}>{strings('bonus.scratch_desc')}</Text>

            <View style={[s.playAdHold]}>
              <FontAwesomeIcon
                icon={ faAd }
                color={'white'}
                size={ 25 } />
            </View>

          </TouchableOpacity>


          <TouchableOpacity
            style={ ( bonusVar.time !== undefined ? [{ transform: [{ scale: 0 }], height: 0, position: 'absolute' }] : [s.gridItem, { backgroundColor: '#1e86ba', borderTopLeftRadius: 0, alignItems: 'flex-start', padding: 20, position: 'relative', marginTop: 35 }] ) }
            onPress={() => { runAd( 'time' ); }}>

            <View style={[s.iconBonusHold, {backgroundColor: '#1e86ba'}]}>
              <FontAwesomeIcon
                icon={ faClock }
                color={'white'}
                size={ 30 } />
            </View>

            <Text style={[s.title, {textAlign: 'left'}]}>{strings('bonus.time_title')}</Text>
            <Text style={{color: 'white'}}>{strings('bonus.time_desc')}</Text>

            <View style={[s.playAdHold]}>
              <FontAwesomeIcon
                icon={ faAd }
                color={'white'}
                size={ 25 } />
            </View>

          </TouchableOpacity>


        </View>
      </ScrollView>

      </ImageBackground>
    </View>
  )
}
