import React, {useState} from 'react';
import { View, Text, ScrollView, TouchableOpacity, Alert, FlatList, ImageBackground, Dimensions, Platform } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

import { Button } from 'react-native-elements';

import Rate, { AndroidMarket } from 'react-native-rate';

import analytics from '@react-native-firebase/analytics';

/* strings */
import { strings } from '../locales/i18n';

import { background } from '../framework/functions.js';

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faCheck, faTimes, faStar } from '@fortawesome/free-solid-svg-icons'

import moment from 'moment';

import { useIsFocused, useFocusEffect } from '@react-navigation/native';
import { RestartLevel, TimeDiff, GetLevelStatus, GetGuessedLogos, isLocked, playEndAd } from '../framework/functions.js';

var s = require('../framework/styles');

import CountDown from '../framework/component/countdown.js';
import ProgressBar from '../framework/component/progressbar.js';
import IconState from '../framework/component/iconstate.js';
import Congratulation from '../framework/component/congratulation.js';

import { InterstitialAd, TestIds, AdEventType } from '@react-native-firebase/admob';

const adUnitId = __DEV__ ? TestIds.INTERSTITIAL : 'ca-app-pub-2436333425882202/5177268588';

const interstitial = InterstitialAd.createForAdRequest(adUnitId, {
  requestNonPersonalizedAdsOnly: true,
});

export function LevelScreen({route, navigation}){
  const isFocused = useIsFocused();

  const [ endModal, setEndModal ] = useState( false );
  const [ levelEnded, setLevelEnded ] = useState( false );

  const [loaded, setLoaded] = useState( false );

  const [ showRateButton, setShowRateButton ] = useState( false );

  const [ totalGuessedLogo, setTotalGuessedLogo ] = useState( 0 );

  useFocusEffect( () => {
    global.CurrentScreen = route.name;

    var endModalVar = false,
        levelEndedVar = false;

    if( route.params !== undefined ){
      if( route.params.endModal !== undefined && route.params.levelEnded !== undefined ){
        var endModalVar = route.params.endModal,
            levelEndedVar = route.params.levelEnded;
      }
    }

    setLevelEnded( levelEndedVar );
    setEndModal( endModalVar );

    setTotalGuessedLogo( GetGuessedLogos() );

    interstitial.load();

    if( global.firstLaunch === false && global.alreadyVoted === false ){
      setShowRateButton(true);
    }
  });


  const runLevelPrompt = ( level ) => {

    if( isLocked( level ) ){
      var NeedForUnlock = global.Level.Levels[ level ].minimum;
      Alert.alert(
        strings('level.alert_locked_title'),
        strings('level.alert_locked_desc', {count: NeedForUnlock})
      );
      return false;
    }

    var levelTime = global.TIMES[['level_' + level]];
    if( levelTime !== undefined ){
      if( TimeDiff( levelTime.end ) === false ){
        levelTime = undefined;
      }
    }

    if( levelTime === undefined ){

      Alert.alert(
        strings('level.alert_start_title'),
        strings('level.alert_start_desc', {limitMinutes: global.levelLimitMinutes}),
        [
          { text: 'OK', onPress: () => { if( level % 2 == 0 ) { runLevelOk( level ) } else { runLevelAd( level ) } } },
          { text: strings('global.cancel') }
        ],
        { cancelable: false }
      );

    } else {
      navigation.navigate('Stage', { level: level });
    }

  }

  const runLevelAd = ( level ) => {

    interstitial.show();
    runLevelOk( level );


  }

  const runLevelOk = ( level ) => {

    RestartLevel( level );
    AsyncStorage.getItem('@TIMES', (err, result) => {

      var indexSeenArray = global.seenModals.indexOf(level);
      if (indexSeenArray !== -1) global.seenModals.splice(indexSeenArray, 1);

      var startTime = moment().format();
      var endTime = moment().add(global.levelLimitMinutes ,'minutes').format();
      var time = {start: startTime, end: endTime};

      if( result === null ){
        var writeStartTime = new Object();
      } else {
        writeStartTime = JSON.parse( result );
      }

      writeStartTime['level_' + level ] = time;

      global.TIMES = writeStartTime;
      AsyncStorage.setItem('@TIMES', JSON.stringify(writeStartTime), () => {

        analytics().logEvent('runLevel', {
          level: level,
        });

        navigation.navigate('Stage', { level: level });

      });

    });
  }

  const countStats = ( levelCounter ) => {
    var levelWork = global.Level.Levels[ levelCounter ];
    var levelStatus = global.ANS[['level_' + levelCounter]];

    if( global.END[['level_' + levelCounter]] !== undefined ){

      countSuccess = global.END[['level_' + levelCounter]].success;
      countFailed = global.END[['level_' + levelCounter]].fail;

    } else {
      var countSuccess = 0;
      var countFailed = 0;

      if( levelStatus !== undefined ){

        for( let item in levelStatus ){
          //console.log( levelStatus[item].result );
          if( levelStatus[item].result == true ){
            countSuccess++;
          } else {
            countFailed++;
          }
        }

        /*levelStatus.map( (data, index) => {
          if( data.result == true ){
            countSuccess++;
          } else {
            countFailed++;
          }
        } );*/
      }
    }

    if( (countSuccess + countFailed) == 0 ){
      var percentage = 0;
    } else {
      var percentage = Math.floor( ( ( countSuccess ) / levelWork.logo.length ) * 100 );
    }

    var arr = { total: levelWork.logo.length,
                success: countSuccess,
                fail: countFailed,
                percentage: percentage
              };
    return arr;
  }

  function RenderItem({data, i}){

    var showElement = true;
    if( data.adult !== undefined && data.adult == true ){
      if( global.BONUS.adult === undefined ){
        var showElement = false;
      }
    }


    if( showElement ){

    var stats = ( countStats(i) );

    var levelStatus = global.ANS[['level_' + i]];
    var levelTime = global.TIMES[['level_' + i]];

    if( levelTime === undefined ){
      var levelTime = false;
    }

    if( levelStatus === undefined ){
      var levelStatus = {};
    }

    return (
      <TouchableOpacity
        key={i}
        style={ ( isLocked( i ) ? [s.gridItem, { backgroundColor: data.color, opacity: 0.4 }] : [s.gridItem, { backgroundColor: data.color }] ) }
        onPress={() => { runLevelPrompt(i); }}>

        <CountDown focus={ isFocused } level={ i } style={[ s.countDown ]} bgColor={ data.color } />

        <IconState status={ GetLevelStatus(i) } iconColor={ data.color } style={{ position: 'absolute', top: '10%', right: '5%', backgroundColor: 'rgba(255, 255, 255, 0.5)', width: 40, height: 40, borderRadius: 20, opacity: 1, display: 'flex', justifyContent: 'center', alignItems: 'center', paddingTop: 0 }} />

        <Text style={[s.title]}>{ data.name }</Text>
        <Text style={[s.progress, s.colorWhite]}>{ stats.success } <FontAwesomeIcon icon={ faCheck } size={10} color={ 'white' } style={( Platform.OS === 'ios' ? {} : { marginBottom: 5 } )} /> <Text style={{opacity: 0.5, marginLeft: 5, marginRight: 5}}>/</Text> { stats.fail } <FontAwesomeIcon icon={ faTimes } size={10} color={ 'white' } style={( Platform.OS === 'ios' ? {} : { marginBottom: 5 } )} /> <Text style={{opacity: 0.5, marginLeft: 5, marginRight: 5}}>/</Text> { stats.total } {strings('level.total')}</Text>
        <Text style={[s.colorWhite]}></Text>

        <ProgressBar value={ stats.percentage } bgColor={ data.color } />

      </TouchableOpacity>
    )

  } else {
    return ( <View /> );
  }

  }

  return(
    <View style={[s.container, {paddingBottom: 0, paddingTop: 0}]}>
      <View style={ ( !loaded ? { transform: [{ scale: 0 }], height: 0, position: 'absolute' } : [s.splashLoading] )}>
        <Text style={[s.splashLoadingText]}>{strings('global.loading')}</Text>
      </View>


      { ( endModal == true && isFocused == true ? <Congratulation visible={endModal} waitingForAd={global.adRunning} level={levelEnded} /> : <View /> ) }

      <View style={ [ ( showRateButton === true ? {} : { transform: [{ scale: 0 }], height: 0, display: 'none', position: 'absolute' } ), {display: 'flex', flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', backgroundColor: '#20346b', paddingTop: 10, paddingBottom: 10 } ]}>
        <Text style={{color: 'white', fontWeight: 'bold'}}>{strings('like.sentense')}</Text>
        <Button
            icon={
              <FontAwesomeIcon icon={ faStar } size={10} color={ 'white' } style={{marginRight: 5}}/>
            }
            title={strings('like.rate')} onPress={()=>{
            const options = {
              GooglePackageName:"com.wenam.travelquiz",
              AmazonPackageName:"com.wenam.travelquiz",
              preferredAndroidMarket: AndroidMarket.Google,
              preferInApp:true,
              openAppStoreIfInAppFails:true,
            }
            Rate.rate(options, success=>{
              if (success) {
                AsyncStorage.setItem('alreadyVoted', JSON.stringify(true) );
                global.alreadyVoted = true;
                setShowRateButton(false);
              }
            })
          }} />
          <Button title="&times;" onPress={()=>{
              AsyncStorage.setItem('alreadyVoted', JSON.stringify(true) );
              global.alreadyVoted = true;
              setShowRateButton(false);
            }} />
        </View>

      <ImageBackground source={background} style={s.backgroundImage, {paddingTop: 0, paddingBottom: 0, paddingLeft: 27, paddingRight: 27}}>

      <ScrollView style={{paddingBottom: 30}}>
        <View style={[s.grid, {marginTop: 30, marginBottom: 30}]}>
          <FlatList
            data={ global.Level.Levels }
            initialNumToRender={5}
            maxToRenderPerBatch={10}
            renderItem={({ item, index }) => <RenderItem data={ item } i={ index } />}
            keyExtractor={(item, index) => 'key'+index}
          />
        </View>
      </ScrollView>

      </ImageBackground>
    </View>
  );

}
