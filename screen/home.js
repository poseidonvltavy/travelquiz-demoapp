import * as React from 'react';
import { View, Text, Image, Dimensions, Alert, ImageBackground } from 'react-native';
import { useFocusEffect, useIsFocused } from '@react-navigation/native';

import I18n from 'react-native-i18n';

import {background} from '../framework/functions.js';

/* strings */
import { strings } from '../locales/i18n';

import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';

var s = require('../framework/styles');


export function HomeScreen({route, navigation}) {

  var rated = false;

  useFocusEffect( () => {
    global.CurrentScreen = route.name
  });

  return (
    <View style={[s.container, s.containerHomePage, {height: Dimensions.get('window').height}]}>

      <ImageBackground source={background} style={s.backgroundImage}>

      <View style={[s.HomePageLogo]}>
        <Image
          style={s.logo}
          resizeMode={'contain'}
          source={require('../img/logo.png')}
        />
      </View>

      <View style={[s.HomePageMenu]}>
        <Button
          title={strings('home.play')}
          buttonStyle={s.button}
          onPress={() => navigation.navigate('Level')}
        />

        <View style={{marginTop: 20}}>
          <Button
            title={strings('home.how_to_play')}
            buttonStyle={s.button}
            onPress={() => navigation.navigate('HowToPlay')}
          />
        </View>

        <View style={{marginTop: 20}}>
          <Button
            title={strings('home.bonus')}
            buttonStyle={[s.button, {backgroundColor: '#B80000' }]}
            onPress={() => navigation.navigate('Bonus')}
          />
        </View>
      </View>


      <Text style={{position: 'absolute', color: 'white', bottom: -30, width: Dimensions.get('window').width, textAlign: 'center'}}>
        Created by Studio Wenam.cz
      </Text>

      </ImageBackground>

    </View>
  );
}
