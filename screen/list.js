import React, {useState, useEffect} from 'react';
import { View, Text, Button, Image, ScrollView, TouchableOpacity, FlatList, ImageBackground } from 'react-native';
import { useIsFocused, useFocusEffect } from '@react-navigation/native';

/* strings */
import { strings, localization } from '../locales/i18n';

import {background} from '../framework/functions.js';

var s = require('../framework/styles');

import { IsGuessed } from '../framework/functions.js';
import CountDown from '../framework/component/countdown.js';


export function StageScreen({route, navigation}){
  const { level } = route.params;
  const isFocused = useIsFocused();
  const levelColor = global.Level.Levels[level].color;

  useFocusEffect( () => {
    global.CurrentScreen = route.name;
    global.ActiveLevel = level;
  });

  /*const renderLogos = () => {

    return global.Level.Levels[level].logo.map((data, key) => {
      var Guessed = IsGuessed( level, data.title );

      return(
        <TouchableOpacity
          onPress={ () => { navigation.navigate( 'Guess', { 'level': level, 'key': key } ); } }
          style={[s.logoGridItem]} key={key}>
          <Image
            resizeMode={ Guessed.guessed && Guessed.result ? 'contain' : 'cover' }
            style={{ width: '100%', height: '100%', flex: 1, alignSelf: 'center' }}
            blurRadius={ Guessed.guessed && Guessed.result ? 0 : 50 }
            source={ data.image }
          />
          <Image
            style={( Guessed.guessed && !Guessed.result ? [s.falseIcon] : [s.falseIcon, { transform: [{ scale: 0 }] }] ) }
            source={ require('../img/false.png') }
          />
        </TouchableOpacity>
      )
    } );

  }*/

  function RenderLogos({data, i}){
    var Guessed = IsGuessed( level, ( localization === 'cs-CZ' ? data.title : data.title_en ) );

    return(
      <TouchableOpacity
        onPress={ () => { navigation.navigate( 'Guess', { 'level': level, 'key': i } ); } }
        style={[s.logoGridItem]} key={i}>
        <Image
          resizeMode={ Guessed.guessed && Guessed.result ? 'contain' : 'cover' }
          style={{ width: '100%', height: '100%', flex: 1, alignSelf: 'center' }}
          blurRadius={ Guessed.guessed && Guessed.result ? 0 : 50 }
          source={ data.image }
        />
        <Image
          style={( Guessed.guessed && !Guessed.result ? [s.falseIcon] : [s.falseIcon, { transform: [{ scale: 0 }] }] ) }
          source={ require('../img/false.png') }
        />
      </TouchableOpacity>
    )
  }

  return(
    <View style={[s.container]}>
      <ImageBackground source={background} style={s.backgroundImage}>

      <CountDown focus={ isFocused } level={ level } style={[ s.countDownPullRight ]} bgColor={ levelColor } />

      <View>
        <Text style={[s.bigTitle]}>{ global.Level.Levels[level]['name'] }</Text>
        <View style={[s.undertitle, { backgroundColor: global.Level.Levels[level]['color'] }]}></View>
        <ScrollView>
          <FlatList
            contentContainerStyle={[s.logoGrid]}
            numColumns={4}
            initialNumToRender={0}
            maxToRenderPerBatch={10}
            data={ global.Level.Levels[level].logo }
            renderItem={({ item, index }) => <RenderLogos data={ item } i={ index } />}
            keyExtractor={(item, index) => 'key'+index}
          />
        </ScrollView>
      </View>

      </ImageBackground>
    </View>
  );
}
