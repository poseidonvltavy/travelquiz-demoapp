import React, {useState, Component} from 'react';
import { View, Text, Image, Dimensions, Alert, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { useFocusEffect, useIsFocused } from '@react-navigation/native';

/* strings */
import { strings, localization } from '../locales/i18n';
import CoverArea from '../framework/component/cover-area.js';

import { Button } from 'react-native-elements';
import { IsGuessed, GetLextLogo, GetLevelStatus, EndLevel } from '../framework/functions.js';

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faCheck, faTimes, faEmpty } from '@fortawesome/free-solid-svg-icons'
import CountDown from '../framework/component/countdown.js';

var s = require('../framework/styles');

export function GuessScreen({route, navigation}) {
  const isFocused = useIsFocused();

  const { level } = route.params;
  const [ key, setKey ] = useState( route.params.key );

  const [ showLogo, setShowLogo ] = useState( false );

  const [ showModal, setShowModal ] = useState( false );
  const [ showModalStatement, setShowModalStatement ] = useState( null );

  const levelItem = ( global.Level.Levels[level]['logo'][key] );
  const [ workingCounter, setWorkingCounter ] = useState( 0 );

  const levelColor = global.Level.Levels[level].color;

  var thisTitle = ( localization === 'cs-CZ' ? levelItem.title : levelItem.title_en );

  var itemGuessed = IsGuessed( level, thisTitle );

  useFocusEffect( () => {
    global.CurrentScreen = route.name;
    global.ActiveLevel = level;

    if( itemGuessed.guessed && itemGuessed.result ){
      setShowLogo( true );
    } else {
      setTimeout( () => {
        setShowLogo( true )
      }, 1000 );
    }

  });

  function hashCode(s){
    return s.split("").reduce(function(a,b){a=((a<<5)-a)+b.charCodeAt(0);return a&a},0);
  }

  function shuffle(a) {
      for (let i = a.length - 1; i > 0; i--) {
          const j = Math.floor(Math.random() * (i + 1));
          [a[i], a[j]] = [a[j], a[i]];
      }
      return a;
  }

  function getRandomOptions(arr, n, first) {
    shuffle( arr );
    var newArr = arr.sort(function(x,y){ return x == first ? -1 : y == first ? 1 : 0; });;
    var newArr = arr.splice(0,4);
    shuffle( newArr );
    return newArr;
  }

  function renderGuessButtons(){
    var WorkingArray = [];
    global.Level.Levels[level].logo.map((data,key) =>
      {
        WorkingArray.push( ( localization === 'cs-CZ' ? data.title : data.title_en ) );
      }
    );

    function buttonNextKey(){
      if( GetLextLogo( level, key ) == false ){
            navigation.navigate('Stage', { level: level } );
          } else {
            setWorkingCounter( 1 );
            setKey( GetLextLogo( level, key ) ); }
    }

    const options = getRandomOptions( WorkingArray, 2, thisTitle );
    if( IsGuessed( level, thisTitle ).guessed ){

      if( IsGuessed( level, thisTitle ).result ){

        return(
          <View style={[s.prepareText, { padding: 0, fontSize: 20, marginTop: 30, justifyContent: 'center', display: 'flex', height: '100%', width: Dimensions.get('window').width, alignItems: 'center' }]}>
            <Text style={[{ padding: 0, marginTop: 0, fontSize: 40, color: 'white' }]}>{strings('guess.congratulation')}</Text>
            <Text style={[{ padding: 0, marginTop: 0, fontSize: 18, color: 'white' }]}>{"\n"}{strings('guess.congratulation_desc')}</Text>
            <TouchableOpacity
              style={[s.guessOptButton, { textAlign: 'center', marginTop: 30, width: 200 }]}
              onPress={ () => { buttonNextKey() } }
              >
              <Text style={{textAlign: 'center'}}>{strings('guess.continue')}...</Text>
            </TouchableOpacity>
          </View>
        );

      } else {

        return(
          <View style={[s.prepareText, { padding: 0, fontSize: 20, marginTop: 30, justifyContent: 'center', display: 'flex', height: '100%', width: Dimensions.get('window').width, alignItems: 'center' }]}>
            <Text style={[{ padding: 0, marginTop: 0, fontSize: 40, color: 'white' }]}>{strings('guess.unfortunately')}</Text>
            <Text style={[{ padding: 0, marginTop: 0, fontSize: 18, color: 'white' }]}>{"\n"}{strings('guess.unfortunately_desc')}</Text>
            <TouchableOpacity
              style={[s.guessOptButton, { textAlign: 'center', marginTop: 30, width: 200 }]}
              onPress={ () => { buttonNextKey() } }
              >
              <Text style={{textAlign: 'center'}}>{strings('guess.continue')}...</Text>
            </TouchableOpacity>
          </View>
        );

      }

    } else {

      if( showLogo ){
        return options.map((data, key) => {
          return(
            <TouchableOpacity
              key={key}
              style={ ( showModal == false ? [s.guessOptButton] : [s.guessOptButton, { transform: [{ scale: 0 }] }] ) }
              onPress={() => GuessItem( data )}
              >
              <Text style={[ {textAlign: 'center', fontSize: ( data.length > 42 ? 11 : 14 )} ]}>{ data }</Text>
            </TouchableOpacity>
          )
        } );
      } else {
        return(
          <View style={[s.prepareText, { padding: 0, fontSize: 20, marginTop: 30, marginLeft: -15, justifyContent: 'center', display: 'flex', alignItems: 'center', justifyContent: 'center' }]}>
            <Text style={[{ padding: 0, marginTop: 60, color: 'white', fontSize: 40 }]}>{strings('global.loading')}</Text>
          </View>
        );
      }


    }

  }

  function GuessItem(tryAnswer){

    if( tryAnswer == thisTitle ){

      setShowModal( true );
      setShowModalStatement(true);

    } else {

      setShowModal(true);
      setShowModalStatement(false);

    }

    if( tryAnswer == thisTitle ){
      var resultVal = true;
    } else {
      var resultVal = false;
    }

    global.ANS[ ["level_" + level] ].push( {name: thisTitle, result: resultVal} );
    AsyncStorage.setItem('@ANS', JSON.stringify( global.ANS ), () => {

      var NextKey = GetLextLogo( level, key );
      if( NextKey  == false ){
        EndLevel( level );
      } else {

        setTimeout(function(){

          setKey( NextKey );
          setWorkingCounter( (workingCounter+1) );

          setShowModal(false);
          setShowModalStatement(null);
        }, 1000);

      }
    });

  }

  function countStats( levelCounter ){
    var levelWork = global.Level.Levels[ levelCounter ];
    var levelStatus = global.ANS[['level_' + levelCounter]];

    if( global.END[['level_' + levelCounter]] !== undefined ){

      countSuccess = global.END[['level_' + levelCounter]].success;
      countFailed = global.END[['level_' + levelCounter]].fail;

    } else {
      var countSuccess = 0;
      var countFailed = 0;

      if( levelStatus !== undefined ){

        for( let item in levelStatus ){
          //console.log( levelStatus[item].result );
          if( levelStatus[item].result == true ){
            countSuccess++;
          } else {
            countFailed++;
          }
        }
      }
    }

    var arr = { total: levelWork.logo.length,
                success: countSuccess,
                fail: countFailed,
              };
    return arr;
  }

  var stats = ( countStats(level) );

  return (
      <View style={[s.guessContainer]}>
        <CountDown focus={ isFocused } level={ level } style={[ s.countDownPullRight ]} bgColor={ levelColor } />

        <View
          style={ ( showModal == true && showModalStatement == false ? [s.modal, { backgroundColor: levelColor }] : [s.modal, { backgroundColor: levelColor, transform: [{ scale: 0 }] }] )}
          pointerEvents={'none'}
          >
          <View style={ s.modalInner }>
            <FontAwesomeIcon icon={ ( showModalStatement == true ? faCheck : faTimes ) } size={115} color={ ( showModalStatement == true ? 'green' : 'red' ) }  style={ s.modalInnerChar } />
          </View>
        </View>

        <View
            style={[s.logoArea]}
          >
          <CoverArea
              hide={ itemGuessed }
              element={ levelItem }
              color={ levelColor }
              elementkey={ key }
              opacity={ ( showModal == true && showModalStatement == true ? false : true ) }
              workingCounter={ workingCounter } />
          <View style={[s.logoHolder]}>
            <Image
              resizeMode={'cover'}
              style={{ width: '100%', height: '100%', flex: 1, alignSelf: 'center', maxWidth: 500, opacity: ( showLogo ? 1 : 0 ) }}
              source={ levelItem.image }
            />
          </View>
        </View>

        <View style={[s.guessOptions, { backgroundColor: levelColor }]}>
          <View style={{position: 'absolute', borderTopLeftRadius: 10, textAlign: 'center', right: 0, height: 100, top: -100, paddingRight: '20%', backgroundColor: levelColor, width: 60, display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
            <View style={{paddingRight: 12}}><Text style={{textAlign: 'right', color: 'white', padding: 5, paddingLeft: 10, height: 30, display: 'flex', justifyContent: 'center'}}>{stats.success}</Text><FontAwesomeIcon icon={ faCheck } size={10} color={ 'white' } style={ { marginLeft: 0, height: 30, position: 'absolute', right: 0, top: 8 } } /></View>
            <View style={{paddingRight: 12}}><Text style={{textAlign: 'right', color: 'white', padding: 5, paddingLeft: 10, height: 30}}>{stats.fail}</Text><FontAwesomeIcon icon={ faTimes } size={10} color={ 'white' } style={ { marginLeft: 0, height: 30, position: 'absolute', right: 0, top: 8 } } /></View>
            <View><Text style={{textAlign: 'right', color: 'white', padding: 5, paddingLeft: 10, height: 30, fontWeight: 'bold'}}>{(stats.success + stats.fail)}/<Text style={{fontSize: 8}}>{stats.total}</Text></Text></View>
          </View>
          <View style={ ( showModal == true && showModalStatement != null ? [{ transform: [{ scale: 0 }] }] : [s.guessOptionsInner] )}>
            { renderGuessButtons() }
          </View>
          <View style={ ( showModal == true && showModalStatement != null ? [] : [{ transform: [{ scale: 0 }] }] )}>
            <Text style={s.prepareText}>{strings('guess.get_ready')}</Text>
          </View>
        </View>
      </View>
  );
}
