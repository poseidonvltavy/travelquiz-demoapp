import React, {useState, Component} from 'react';
import { View, Text, Image, Dimensions, Alert, TouchableOpacity, ScrollView, ImageBackground} from 'react-native';

/* strings */
import { strings } from '../locales/i18n';

import {background} from '../framework/functions.js';

import AsyncStorage from '@react-native-community/async-storage';
import { useFocusEffect, useIsFocused } from '@react-navigation/native';

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faClock, faEye, faBan } from '@fortawesome/free-solid-svg-icons'

var s = require('../framework/styles');

export function HowToPlayScreen({route, navigation}) {
  const isFocused = useIsFocused();

  useFocusEffect( () => {
    global.CurrentScreen = route.name
  });

  return(
    <View style={[s.container]}>
      <ImageBackground source={background} style={s.backgroundImage}>

        <ScrollView>
          <Text style={[s.bigTitle, {textAlign: 'left'}]}>{strings('howtoplay.game_point_title')}</Text>
          <View style={[s.undertitle, {backgroundColor: '#235fba', marginLeft: 0}]} />

          <Text style={{marginTop: 20, color: '#e8e8e8'}}>{strings('howtoplay.game_point_desc')}</Text>

          <Text style={[s.bigTitle, {textAlign: 'left', marginTop: 30}]}>{strings('howtoplay.params_title')}</Text>
          <View style={[s.undertitle, {backgroundColor: '#235fba', marginLeft: 0}]} />

          <Text style={{marginTop: 20, color: '#e8e8e8'}}>{strings('howtoplay.params_desc_1')}</Text>
          <Text style={{marginTop: 20, color: '#e8e8e8'}}>{strings('howtoplay.params_desc_2')}</Text>

          <Text style={[s.bigTitle, {textAlign: 'left', marginTop: 30}]}>{strings('howtoplay.dont_forget_title')}</Text>
          <View style={[s.undertitle, {backgroundColor: '#235fba', marginLeft: 0}]} />

          <Text style={{marginTop: 20, color: '#e8e8e8'}}>{strings('howtoplay.dont_forget_desc')}</Text>
        </ScrollView>

      </ImageBackground>
    </View>
  )
}
