// In App.js in a new project
import * as React from 'react';
import { View, Text, Button } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import { navigationRef } from './RootNavigation';

import analytics from '@react-native-firebase/analytics';

import { CardStyleInterpolators } from '@react-navigation/stack';

import { YellowBox } from 'react-native';
YellowBox.ignoreWarnings(['VirtualizedLists should never be nested']);

/* strings */
import { strings } from './locales/i18n';

/* INITIAL SCRIPTS */
import {initDefaultVariables} from './framework/functions.js';

/* SCREENS */
import {HomeScreen} from './screen/home.js';
import {LevelScreen} from './screen/level.js';
import {StageScreen} from './screen/list.js';
import {GuessScreen} from './screen/guess.js';
import {BonusScreen} from './screen/bonus.js';
import {HowToPlayScreen} from './screen/howtoplay.js';

const Stack = createStackNavigator();

// Gets the current screen from navigation state
const getActiveRouteName = state => {
  const route = state.routes[state.index];

  if (route.state) {
    // Dive into nested navigators
    return getActiveRouteName(route.state);
  }

  return route.name;
};

function App() {
  initDefaultVariables();

  const routeNameRef = React.useRef();
  React.useEffect(() => {
    const state = navigationRef.current.getRootState();

    // Save the initial route name
    routeNameRef.current = getActiveRouteName(state);
  }, []);

  return (
    <NavigationContainer
      ref={navigationRef}
      onStateChange={(state) => {
        const previousRouteName = routeNameRef.current;
        const currentRouteName = getActiveRouteName(state);

        if (previousRouteName !== currentRouteName) {
          analytics().setCurrentScreen(currentRouteName, currentRouteName);
        }
      }}
    >
    <Stack.Navigator
        initialRouteName="Home"
        >
        <Stack.Screen name="Home"
                      options={{headerShown: false,
                                title: strings('navigation.home')}}
                      component={HomeScreen}
                      />
        <Stack.Screen name="Level"
                      options={{headerShown: true,
                                title: strings('navigation.select_level'),
                                headerStyle: { backgroundColor: '#085078' },
                                headerTitleStyle: { color: 'white' },
                                headerTintColor: '#ffffff'}}
                      component={LevelScreen} />
        <Stack.Screen name="Stage"
                      options={{headerShown: true,
                                title: strings('navigation.list'),
                                headerStyle: { backgroundColor: '#085078' },
                                headerTitleStyle: { color: 'white' },
                                headerTintColor: '#ffffff'}}
                      component={StageScreen} />
        <Stack.Screen name="Guess"
                      options={{headerShown: true,
                                title: strings('navigation.guess_a_location'),
                                headerStyle: { backgroundColor: '#085078' },
                                headerTitleStyle: { color: 'white' },
                                headerTintColor: '#ffffff'}}
                      component={GuessScreen} />
        <Stack.Screen name="Bonus"
                      options={{headerShown: true,
                                title: strings('navigation.game_bonus'),
                                headerStyle: { backgroundColor: '#085078' },
                                headerTitleStyle: { color: 'white' },
                                headerTintColor: '#ffffff'}}
                      component={BonusScreen} />
        <Stack.Screen name="HowToPlay"
                      options={{headerShown: true,
                                title: strings('navigation.howtoplay'),
                                headerStyle: { backgroundColor: '#085078' },
                                headerTitleStyle: { color: 'white' },
                                headerTintColor: '#ffffff'}}
                      component={HowToPlayScreen} />

      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
