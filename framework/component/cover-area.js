import React, {Component} from 'react';
import { View, Text, Dimensions } from 'react-native';

import ScratchView from 'react-native-scratch'


var s = require('../styles');

export default class CoverArea extends Component{
  constructor( props ){
      super();

      this.maxShow = global.maximumLenghtToScrath;

      this.state = {
        workingCounter: 0,
        statusBarWidth: 0,
        color: props.color,
        itemGuessed: props.hide,

        element: props.element,
        opacity: props.opacity,

        elementTopOffset: 0,
        elementWidth: 0,
        locked: false
      };

  }

  UNSAFE_componentWillUpdate(props){
    if( props.workingCounter !== this.state.workingCounter ){
      this.setState({ workingCounter: props.workingCounter,
                      statusBarWidth: 0,
                      status: 0,
                      locked: false,
                      opacity: props.opacity,
                      itemGuessed: props.hide
                    });
    }

    if( props.opacity !== this.state.opacity ){
      this.setState({ opacity: props.opacity });
    }
  }

	onScratchProgressChanged = ({ value, id }) => {
    var totalWidth =  (Dimensions.get('window').width / 100) * ( ( value / global.maximumLenghtToScrath ) * 100);
    this.setState({ statusBarWidth: totalWidth });

    if( value > global.maximumLenghtToScrath ){
      this.setState({ locked: true });
    }
	}

	onScratchDone = ({ isScratchDone, id }) => {
		this.setState({ locked: true });
	}

	onScratchTouchStateChanged = ({ id, touchState }) => {
		// Example: change a state value to stop a containing
		// FlatList from scrolling while scratching
		this.setState({ scrollEnabled: !touchState });
	}

  render(){

    return(
      <View
        pointerEvents={ ( this.state.locked === true ? 'none' : 'auto' ) }
        style={ ( ( this.state.itemGuessed && this.state.itemGuessed.result ) || this.state.opacity === false ? [s.coverPointContainer, { opacity: 0 }] : [s.coverPointContainer] )}
        key={ this.state.workingCounter }
        >
        <View style={ s.statusBar }>
          <View style={ [s.statusBarFill, {width: this.state.statusBarWidth } ]  }></View>
        </View>

        <ScratchView
        				key={ this.state.workingCounter } // ScratchView id (Optional)
        				id={ this.state.workingCounter } // ScratchView id (Optional)
        				brushSize={22} // Default is 10% of the smallest dimension (width/height)
        				threshold={ global.maximumLenghtToScrath } // Report full scratch after 70 percentage, change as you see fit. Default is 50
        				fadeOut={false} // Disable the fade out animation when scratch is done. Default is true
        				placeholderColor="#D1D5D4" // Scratch color while image is loading (or while image not present)
        			  onTouchStateChanged={this.onTouchStateChangedMethod} // Touch event (to stop a containing FlatList for example)
        				onScratchProgressChanged={this.onScratchProgressChanged} // Scratch progress event while scratching
        				onScratchDone={this.onScratchDone} // Scratch is done event
        			/>

      </View>
    )
  }
}
