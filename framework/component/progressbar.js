import React, {
    Component
} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Animated,
    Dimensions
} from 'react-native';

export default class ProgressBar extends Component {
    constructor( props ){
      super( props );
      this.state = {
          progressBarvalue: 0,
          bgColor: this.props.bgColor,
      };
    }

    //initialisation for the progress bar
    componentDidMount() {
        this.setUpValue();
    }

    componentDidUpdate( prevProps ){
      if (prevProps.value !== this.props.value) {
        this.setUpValue();
      }
    }

    setUpValue = () => {
      this.setState({progressBarvalue: this.props.value});
    }

    render() {
        return (
          <View>
            <View style={progressStyles.innerStyle}>
              <View style={[progressStyles.fillBarStyle, { width: ( ( Dimensions.get('window').width * 0.73) * ( this.state.progressBarvalue/100 ) ), backgroundColor: this.state.bgColor } ]} />
              <Text style={progressStyles.label}> { this.state.progressBarvalue}%</Text>
            </View>
          </View>
        );
    }
}

const progressStyles = StyleSheet.create({
  containerStyle: {
    width: "100%",
    height: 40,
    padding: 3,
    borderColor: "#FAA",
    borderWidth: 3,
    borderRadius: 30,
    marginTop: 200,
    justifyContent: "center",
  },
  innerStyle:{
    width: ( Dimensions.get('window').width * 0.72 ),
    height: 16,
    borderRadius: 16,
    backgroundColor: 'white',
    position: 'relative',
    overflow: 'hidden'
  },
  fillBarStyle: {
    height: 16,
    position: 'absolute',
    display: 'flex',
    opacity: 0.4,
    transform: [{ skewX: '-30deg' }],
    marginLeft: -5
  },
  label:{
    fontSize:14,
    color: "black",
    position: "absolute",
    zIndex: 1,
    alignSelf: "center",
  }
});
