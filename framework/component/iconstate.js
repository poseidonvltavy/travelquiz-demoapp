import React, {Component} from 'react';
import { View, Text, Dimensions, Platform } from 'react-native';

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faCheck, faClock, faPlay, faLock } from '@fortawesome/free-solid-svg-icons'

var s = require('../styles');



export default class IconState extends Component{
  constructor( props ){
    super( props );

    this.state = {
      status: props.status,
      icon: 'faClock'
    };
  }

  redefineStatus = (props) => {
    var status = this.props.status;
    if( status == 'active' ){
      var icon = <FontAwesomeIcon
          icon={ faPlay }
          size={24} color={ 'white' }
          style={{ marginBottom: 5 }} />;
    } else if( status == 'end' ){
      var icon = <FontAwesomeIcon
          icon={ faCheck }
          size={24} color={ 'white' }
          style={{ marginBottom: 5 }} />;
    } else if( status == 'lock' ){
      var icon = <FontAwesomeIcon
          icon={ faLock }
          size={24} color={ 'white' }
          style={{ marginBottom: 5 }} />;
    } else {
      var icon = <FontAwesomeIcon
          icon={ faClock }
          size={24} color={ 'white' }
          style={{ marginBottom: 5 }} />;
    }

    this.setState({
      status: this.props.status,
      icon: icon
    });
  }

  componentDidMount(){
    this.redefineStatus();
  }

  componentDidUpdate( prevProps ){
    if( prevProps.status !== this.props.status ){
      this.redefineStatus();
    }
  }

  render(){
    return(
      <View style={ [this.props.style, { paddingTop: 0 }] }>
        <Text>{ this.state.icon }</Text>
      </View>
    )
  }
}
