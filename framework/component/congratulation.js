import React, {Component} from 'react';
import { View, Text } from 'react-native';
import { Button } from 'react-native-elements';

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faCheck } from '@fortawesome/free-solid-svg-icons'

/* strings */
import { strings } from '../../locales/i18n';


var s = require('../styles');
import Level from '../levels.js';

export default class Congratulation extends Component{
  constructor( props ){
    super( props );

    this.state = {
      visible: this.props.visible,
      level: this.props.level,
      success: 0,
      failed: 0,
      total: 0,
      adRunning: this.props.waitingForAd
    };
  }

  componentDidMount( props ){

    if( global.seenModals.indexOf( this.state.level ) !== -1 ){
      this.setState({ visible: false });
      return;
    }

    this.interval = setInterval( () => {
      this.setState( { adRunning: global.adRunning } );
    }, 500 );

    if( global.END !== undefined ){
      var end = global.END['level_' + this.state.level];
      global.seenModals.push( this.state.level );

      if( end === undefined ){
        this.setState({
          success: 0,
          failed: 0,
          total: 0,
        });
      } else {
        var total = Level.Levels[ this.state.level ].logo.length;
        this.setState({
          success: end.success,
          failed: end.fail,
          total: total,
        });
      }
    }

  }

  componentWillUnmount(){
    if( this.interval !== undefined ) { clearInterval( this.interval ); }
  }

  render(){
    if( this.state.visible === true ){

      return(
        <View
          style={[s.modalCongratulationHolder]}
          >
          <View
            style={[s.modalCongratulation]}
            >
            <Text style={[s.modalCongratulationTitle]}>{strings('congratulation.title')}</Text>
            <Text style={{marginTop: 10, textAlign: 'center'}}>{strings('congratulation.line_1')}</Text>
            <Text style={{marginTop: 0, textAlign: 'center'}}>{strings('congratulation.line_2')}</Text>

            <Text style={{marginTop: 30, textAlign: 'center'}}>{strings('congratulation.success')}: {this.state.success}</Text>
            <Text style={{marginTop: 10, textAlign: 'center'}}>{strings('congratulation.bad')}: {this.state.failed}</Text>
            <Text style={{marginTop: 10, marginBottom: 30, textAlign: 'center'}}>{strings('congratulation.total')}: {this.state.total}</Text>

            <View
              pointerEvents={ ( this.state.adRunning ? 'none' : 'auto' ) }
              >
              <Button
                icon={
                  <FontAwesomeIcon
                    icon={ faCheck }
                    size={16}
                    color={ 'white' }
                    style={{marginRight: 10}} />
                }
                iconLeft
                onPress={() => { clearInterval( this.interval ); this.setState({ visible: false }) }}
                title={strings('congratulation.ok')}
              />
            </View>

          </View>
        </View>
      )

    } else {
      return ( <View/> )
    }
  }
}
