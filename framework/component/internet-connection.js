import React, {
    Component
} from 'react';
import {View, Text} from 'react-native';
import NetInfo from "@react-native-community/netinfo";

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faGlobe } from '@fortawesome/free-solid-svg-icons'

var s = require('../../framework/styles');

export default class DetectInternetConnection extends Component {
    constructor( props ){
      super( props );
      this.state = {
          isOnline: true,
          showOnlineStatus: false,
      };
    }

    //initialisation for the progress bar
    componentDidMount() {
        this.CheckConnectivity();
    }

    CheckConnectivity = () => {
      NetInfo.fetch().then(state => {

        let statenet = state.isInternetReachable;

        this.setState({
          isOnline: statenet,
          showOnlineStatus: true,
        });

        if( statenet ){
          setTimeout( () => {
            this.setState({
              showOnlineStatus: false,
            });
          }, 2000 );
        } else {
          this.CheckConnectivity();
        }

      });
    };

    render() {
        return (
          <View>
            <FontAwesomeIcon icon={ faGlobe } size={30} color={ ( this.state.showOnlineStatus == true ? 'green' : 'green' ) }  style={ [s.connectionStatement, { opacity: ( this.state.showOnlineStatus ? 1 : 0 ) } ] } />
          </View>
        );
    }
}
