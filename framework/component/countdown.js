import React, {Component} from 'react';
import { View, Text } from 'react-native';

import moment from 'moment';
import { TimeDiff } from '../functions.js';

var s = require('../styles');

export default class CountDown extends Component{
  constructor( props ){
    super( props );

    var levelTime = global.TIMES[['level_' + props.level]];
    if( levelTime === undefined ){ var levelTime = false; }

    this.state = {
      timer: null,
      focus: props.focus,
      times: levelTime,
      style: props.style,
      level: this.props.level
    };
  }

  decrementClock = () => {

   if( this.state.times && this.clockCall ){

     if( global.END[['level_' + this.state.level]] !== undefined ){
       var diff = null;
     } else {
       var diff = TimeDiff( this.state.times.end );

       if( diff === false){
         var diff = null;
       } else {
         var diff = this.secondsToTime( diff );
       }
     }

     this.setState((prevstate) => ({ timer: diff }));
     if( this.state.timer == 0 ){
       clearInterval(this.clockCall);
     }
   }
  };

  secondsToTime = ( sec ) => {
      if( sec < 0 ){
        return false;
      }
      var sec_num = parseInt(sec, 10); // don't forget the second param
      var hours   = Math.floor(sec_num / 3600);
      var minutes = Math.floor((sec_num) / 60);
      var seconds = sec_num - (hours * 3600) - (minutes * 60);

      if (hours   < 10) {hours   = "0"+hours;}
      if (minutes < 10) {minutes = "0"+minutes;}
      if (seconds < 10) {seconds = "0"+seconds;}
      return minutes+':'+seconds;
  }


  componentDidMount( props ){
    if( this.state.times ){

      var diff = TimeDiff( this.state.times.end );

      if( diff === false){
        var diff = null;
      } else {
        var diff = this.secondsToTime( diff );
      }

      this.setState((prevstate) => ({ timer: diff }));

      this.clockCall = setInterval(() => {
        this.decrementClock();
      }, 800);
    }
  }

  componentDidUpdate( props ){
    if (props.focus !== this.props.focus) {
      if( props.focus == true ){
        var levelTime = global.TIMES[['level_' + props.level]];
        if( levelTime === undefined ){ var levelTime = false; }

        this.setState({
          times: levelTime
        });

        if( this.clockCall === undefined ){
          this.clockCall = setInterval(() => {
            this.decrementClock();
          }, 1000);
        }
      }
    }
  }

  componentWillUnmount() {
    clearInterval(this.clockCall);
  }

  render(){
    return(
      <View style={ ( this.state.timer === null ? { display: 'none' } : [this.state.style, { backgroundColor: this.props.bgColor }] ) }>
        <Text style={{color: 'white', fontWeight: 'bold', textAlign: 'center', marginTop: 5}}>
        { ( this.state.timer === null ? '' : this.state.timer ) }
        </Text>
      </View>
    );
  }
}
