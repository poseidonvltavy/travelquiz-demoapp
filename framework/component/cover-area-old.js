import React, {Component} from 'react';
import {  View,
          Text,
          Dimensions,
          TouchableWithoutFeedback,
          TouchableOpacity,
          TouchableNativeFeedback,
          PanResponder } from 'react-native';

var s = require('../styles');

export default class CoverArea extends Component{
  constructor( props ){
      super();

      this.maxShow = global.maximumLenghtToScrath;

      this.state = {
        points: [],
        workingCounter: null,
        statusBarWidth: 0,
        color: props.color,
        itemGuessed: props.hide,

        element: props.element,

        elementTopOffset: 0,
        elementWidth: 0,
      };

      this._panResponder = PanResponder.create({
        onMoveShouldSetResponderCapture: () => true,
        onMoveShouldSetPanResponderCapture: () => true,

        onPanResponderMove: (e, state) => {
          let blockSize = this.state.elementWidth / 15;
          let x = state.moveX;
          let y = state.moveY;

          let calHorizontal = ( Math.round( x / blockSize ) );
          let calVertical = ( Math.round( (y - this.state.elementTopOffset ) / blockSize ) );
          let calResult = ((calVertical * 15) + calHorizontal);

          this.addToList(calResult)
        }
      });

  }

  componentDidMount(){

    setTimeout( () => {
      this.marker.measure( (fx, fy, width, height, px, py) => {

          this.setState({elementWidth: width})
          this.setState({elementTopOffset: py})

      })
    }, 300);

  }

  /*componentDidUpdate( props ){
    if( props.workingCounter !== this.state.workingCounter ){
      this.setState({ points: [],
                      workingCounter: props.workingCounter,
                      statusBarWidth: 0,
                      itemGuessed: props.hide
                    });
    }
  }*/

  UNSAFE_componentWillUpdate(props){
    if( props.workingCounter !== this.state.workingCounter ){
      this.setState({ points: [],
                      workingCounter: props.workingCounter,
                      statusBarWidth: 0,
                      itemGuessed: props.hide
                    });
    }
  }

  addToList = ( i ) => {
    var Arr = this.state.points;

    if( this.state.points.indexOf( i ) !== -1 ){
      return false;
    }

    if( this.state.itemGuessed.guessed === true && this.state.itemGuessed.result === false ){
      return false;
    }

    if( Arr.length < this.maxShow ){
      Arr.push(i);

      // Count Width of Status bar
      var totalWidth =  (Dimensions.get('window').width / 100) * ( ( Arr.length / this.maxShow ) * 100);
      this.setState({ points: Arr, statusBarWidth: totalWidth });
    }
  }

  renderElements = () => {
    var mapArray = Array.from(Array( global.renderScratchElements ).keys());
    return mapArray.map((data, i) => {
      return (
        <TouchableWithoutFeedback
        onPress={ () => { this.addToList(i); } }

        pointerEvents={ ( this.state.points.indexOf( i ) == -1 ? 'auto' : 'none' ) }
        key={i}
        >
          <View
            style={ ( this.state.points.indexOf( i ) == -1 ? [s.coverPoint] : [s.coverPointHidden] ) } />
        </TouchableWithoutFeedback>
      )
    });
  }

  render(){
    return(
      <View
        {...this._panResponder.panHandlers}
        ref={(ref) => { this.marker = ref }}
        onLayout={({nativeEvent}) => {
          if (this.marker) {
            this.marker.measure((x, y, width, height, pageX, pageY) => {
              this.setState({elementWidth: width})
              this.setState({elementTopOffset: pageY})
            })
          }
        }}
        style={ (this.state.itemGuessed && this.state.itemGuessed.result ? [s.coverPointContainer, { opacity: 0 }] : [s.coverPointContainer] )}
        >
        <View style={ s.statusBar }>
          <View style={ [s.statusBarFill, {width: this.state.statusBarWidth } ]  }></View>
        </View>
        { this.renderElements() }
      </View>
    )
  }
}
