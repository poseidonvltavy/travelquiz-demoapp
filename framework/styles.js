'use strict';

import {Dimensions} from 'react-native';


var React = require('react-native');

var {
  StyleSheet,
} = React;

module.exports = StyleSheet.create({

  displayNone:{
    display: 'none'
  },

  backgroundImage: {
    flex: 1,
    flexDirection: "column",
    height: Dimensions.get('window').height,
    padding: 27,
    paddingLeft: 27,
    paddingRight: 27
  },

  container: {
      backgroundColor: 'white',
      height: Dimensions.get('window').height,
      padding: 0,
      paddingTop: 0,
      flex: 1,
  },

  containerHomePage:{
    paddingTop: 0,
    display: 'flex',
    flexDirection: 'column',
    paddingBottom: 100
  },

  button: {
    padding: 20,
    backgroundColor: '#0a748f',
    shadowColor: "#000",
    shadowOffset: {
    	width: 0,
    	height: 12,
    },
    shadowOpacity: 0.8,
    shadowRadius: 24.00,

    elevation: 28,
  },

  button_home: {

  },

  HomePageLogo:{
    height: '33%',
    display: 'flex',
    flex: 2,
    justifyContent: 'center',
  },

  HomePageMenu:{
    height: '33%',
    display: 'flex',
    flex: 3,
    justifyContent: 'flex-end',
  },

  logo: {
    alignSelf: 'center',
    width: '100%'
  },

  grid: {
    justifyContent: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
    flex: 1,
  },
  gridItem: {
    margin: 20,
    marginLeft: 0,
    marginRight: 0,
    width: '100%',
    height: 150,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
    color: 'white',
    shadowColor: "#000",
    shadowOffset: {
     width: 0,
     height: 5,
    },
    shadowOpacity: 1,
    shadowRadius: 7,

    elevation: 10,
    borderTopRightRadius: 0,
  },

  iconBonusHold: {
    position: 'absolute',
    top: -35,
    left: 0,
    padding: 15,
    paddingTop: 5,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    paddingBottom: 0
  },

  bigTitle: {
    textAlign: 'center',
    fontSize: 22,
    marginBottom: 10,
    fontWeight: 'bold',
    color: 'white'
  },

  undertitle:{
    width: 100,
    height: 2,
    marginLeft: 'auto',
    marginRight: 'auto'
  },

  title:{
    color: 'white',
    fontSize: 20,
    marginBottom: 20,
    fontWeight: 'bold'
  },

  colorWhite:{
    color: 'white'
  },

  logoGrid:{
    justifyContent: 'space-between',
    flex: 1,
    marginTop: 20,
    margin: -5,
  },

  logoGridItem:{
    width: (Dimensions.get('window').width * 0.21) - 5,
    height: (Dimensions.get('window').width * 0.21) - 5,
    textAlign: 'center',
    backgroundColor: 'red',
    marginBottom: 5,
    borderRadius: 0,
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: '#ededed',
    overflow: 'hidden',
    margin: 5
  },

  falseIcon:{
    position: 'absolute',
    zIndex: 5,
    width: 50,
    height: 50,
    right: 0,
    bottom: 0,
    margin: 'auto',
  },

  guessContainer:{
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    flexDirection: 'column',
    flexWrap: 'wrap',
    flex: 1,
    overflow: 'hidden'
  },

  logoArea:{
    backgroundColor: 'white',
    height: '100%',
    flex: 1,
    width: Dimensions.get('window').width,
    borderWidth: 0,
    borderColor: 'red'
  },

  logoHolder:{
    flex: 1
  },

  guessOptions:{
    height: 250,
    width: Dimensions.get('window').width,
    padding: 0,
    display: 'flex',
    flexWrap: 'wrap',
    flexDirection: 'row'
  },

  guessOptionsInner:{
    width: Dimensions.get('window').width,
    padding: 20,
    display: 'flex',
    flexWrap: 'wrap',
    flexDirection: 'column',
    justifyContent: 'space-between'
  },

  guessOptButton:{
     //width: (Dimensions.get('window').width * 0.4),
     margin: 0,
     marginBottom: 10,
     backgroundColor: 'white',
     padding: 10,
     textAlign: 'center',
     borderRadius: 20,
     width: '100%'
   },

   guessOptButtonResultOK: {
     backgroundColor: 'green',
   },

   guessOptButtonResultOKText: {
     color: 'white',
     fontWeight: 'bold'
   },

   /* MODAL */

   modal: {
     width: 250,
     height: 250,
     backgroundColor: '#ededed',
     display: 'flex',
     position: 'absolute',
     zIndex: 11111,
     alignSelf: 'center',
     top: ( Dimensions.get('window').height * 0.10 ),
     borderRadius: 125,
     justifyContent: 'center'
   },

   modalInner:{
     textAlign: 'center',
     width: 180,
     height: 180,
     borderRadius: 90,
     backgroundColor: 'white',
     marginLeft: 'auto',
     marginRight: 'auto',
     position: 'relative',
     display: 'flex',
     justifyContent: 'center',
     alignItems: 'center'
   },

   modalInnerChar:{
     flex: 1
   },

   modalImage: {
     width: 150,
     height: 150,
     position: 'absolute',
     flex: 1,
     display: 'flex',
     alignSelf: 'center',
     justifyContent: 'center'
   },

   prepareText: {
      textShadowColor: 'rgba(0, 0, 0, 0.75)',
      textShadowRadius: 25,
      color: 'white',
      width: '100%',
      textAlign: 'center',
      fontSize: 30,
      width: (Dimensions.get('window').width - 0),
      marginTop: 100,
      position: 'absolute'
   },

   coverPointContainer:{
      position: 'absolute',
      width: Dimensions.get('window').width,
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'wrap',
      justifyContent: 'flex-start',
      alignItems: 'flex-start',
      zIndex: 5,
      height: ( Dimensions.get('window').height - 250 ),
   },

   coverPoint: {
     width: ( ((Dimensions.get('window').width - 1) / 15) + 0 ),
     height: ( ((Dimensions.get('window').width - 1) / 15) + 1 ),
     backgroundColor: '#D1D5D4',
     marginBottom: -1
   },
   coverPointHidden: {
     width: ( ((Dimensions.get('window').width - 1) / 15) + 0 ),
     height: ( ((Dimensions.get('window').width - 1) / 15) + 1 ),
     backgroundColor: 'transparent',
     marginBottom: -1
   },

   statusBar:{
     position: 'absolute',
     backgroundColor: 'white',
     height: 5,
     width: (Dimensions.get('window').width),
     zIndex: 10
   },
   statusBarFill:{
     position: 'relative',
     backgroundColor: 'red',
     height: 5,
     width: (Dimensions.get('window').width),
     zIndex: 10
   },

   countDown:{
     position: 'absolute',
     top: -20,
     right: 10,
     //transform: [{ rotate: '45deg'}],
     padding: 0,
     color: 'white',
     borderTopLeftRadius: 10,
     borderTopRightRadius: 10,
     marginRight: -10,
     width: 70,
     height: 20,
     textAlign: 'center',
     borderBottomWidth: 0
   },

   countDownPullRight: {
     position: 'absolute',
     right: -85,
     top: -10,
     padding: 25,
     paddingBottom: 5,
     textAlign: 'center',
     width: 200,
     transform: [{ rotate: '45deg'}],
     zIndex: 1000
   },

   progress: {
     fontWeight: 'bold'
   },

   modalCongratulationHolder:{
     width: Dimensions.get('window').width,
     height: Dimensions.get('window').height,
     position: 'absolute',
     top: 0, left: 0,
     backgroundColor: 'rgba(255, 255, 255, 0.8)',
     zIndex: 10000,
     paddingLeft: 120
   },

   modalCongratulation:{
     position: 'absolute',
     zIndex: 50,
     margin: 'auto',
     width: '100%',
     top: '10%',
     height: 320,
     backgroundColor: 'white',
     display: 'flex',
     alignSelf: 'center',
     borderRadius: 20,
     borderColor: '#ededed',
     borderWidth: 1,
     padding: 20,
     marginTop: 30,
     shadowColor: "#000",
     shadowOffset: {
     	width: 0,
     	height: 2,
     },
     shadowOpacity: 0.25,
     shadowRadius: 3.84,

     elevation: 5,
   },

   modalCongratulationTitle:{
     fontSize: 25,
     textAlign: 'center',
     fontWeight: 'bold',
     marginTop: 10
   },

   splashLoading:{
     position: 'absolute',
     width: Dimensions.get('window').width,
     height: Dimensions.get('window').height,
     position: 'absolute',
     top: 0, left: 0,
     backgroundColor: 'rgba(255, 255, 255, 0.8)',
     display: 'flex',
     justifyContent: 'center',
     alignItems: 'center',
     zIndex: 10000,
   },

   splashLoadingText:{
     fontSize: 40,
     fontWeight: 'bold'
   },

   playAdHold:{
     display: 'flex',
     flexDirection: 'row',
     alignItems: 'center',
     alignSelf: 'flex-end',
     borderColor: 'white',
     borderRadius: 10,
     borderWidth: 1,
     overflow: 'hidden',
     height: 20,
     marginTop: 15
   },

   playAd:{
     color: 'white',
     marginRight: 5,
     fontWeight: 'bold',
     fontSize: 12
   },

   connectionStatement: {
     position: 'absolute',
     right: 0, top: 0
   }

});
