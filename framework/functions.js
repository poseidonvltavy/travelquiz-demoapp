import AsyncStorage from '@react-native-community/async-storage';

import { App } from '../App.js'
import * as RootNavigation from '../RootNavigation.js';

import { strings, localization } from '../locales/i18n';

import {Dimensions} from 'react-native';

import { InterstitialAd, TestIds, AdEventType } from '@react-native-firebase/admob';
import analytics from '@react-native-firebase/analytics';

import Level from '../framework/levels.js';
import moment from 'moment';

export const background = require('../img/bg.jpg');

export function initDefaultVariables(){

  global.Level = Level;

  global.showModal = false;
  global.showModalStatement = null;

  global.ActiveLevel = null;

  global.levelLimitMinutes = 5;
  global.maximumLenghtToScrath = 10;

  global.adRunning = false;

  global.firstLaunch = false;
  global.alreadyVoted = true;

  global.seenModals = new Array();

  global.renderScratchElements = ( Math.ceil( ( ( (Dimensions.get('window').height) - 100 / 2) ) / (Dimensions.get('window').width / 15) ) * 15 );


  global.ANS = new Object();
  global.TIMES = new Object();
  global.END = new Object();
  global.BONUS = new Object();

  //AsyncStorage.clear();
  const retrieveAllData = async () => {

    await AsyncStorage.getItem('@ANS', (err, result) => {}).then( res => {
      var foundAnswers = res;
      if(foundAnswers === null){
        var foundAnswersReader = new Object();
      } else {
        var foundAnswersReader = JSON.parse(foundAnswers);
      }

      global.ANS = foundAnswersReader;
    });

    await AsyncStorage.getItem('@TIMES', (err, result) => {
      var foundTimes = result;
      if(foundTimes === null){
        var foundTimesArray = new Array();
      } else {
        var foundTimesArray = JSON.parse(foundTimes);
      }

      global.TIMES = foundTimesArray;
    });

    await AsyncStorage.getItem('@END', (err, result) => {
      var foundEnd = result;
      if(foundEnd === null){
        var foundEndArray = new Object();
      } else {
        var foundEndArray = JSON.parse(foundEnd);
      }

      global.END = foundEndArray;
    });

    await AsyncStorage.getItem('@BONUS', (err, result) => {
      var foundBonus = result;
      if(foundBonus === null){
        var foundBonusArray = new Object();
      } else {
        var foundBonusArray = JSON.parse(foundBonus);
      }

      global.BONUS = foundBonusArray;

      for( let item in foundBonusArray ){
        if( item == 'time' ){
          global.levelLimitMinutes = 10;
        }
        if( item == 'scratch' ){
          global.maximumLenghtToScrath = 13;
        }
      }
    });

    AsyncStorage.getItem("alreadyLaunched").then(value => {
      if(value == null){
        AsyncStorage.setItem('alreadyLaunched', JSON.stringify(true) ); // No need to wait for `setItem` to finish, although you might want to handle errors
        global.firstLaunch = true;
      }
      else{
        global.firstLaunch = false;
      }}
    );

    AsyncStorage.getItem("alreadyVoted").then(value => {
      if(value == null){
        global.alreadyVoted = false;
      }
      else{
        global.alreadyVoted = true;
      }}
    );
  }

  retrieveAllData();


  checkBonuses();
  CheckLevelTimeStatuses();
}

export function addBonus( type ){

  if( type == 'time' || type == 'scratch' ){
    var expire = moment().add(1, 'hour').format('YYYY-MM-DD HH:mm:ss');
    global.BONUS[type] = {expire: expire};
  }

  if( type == 'adult' ){
    var expire = moment().add(10, 'year').format('YYYY-MM-DD HH:mm:ss');
    global.BONUS[type] = {expire: expire};
  }

  if( type == 'time' ){
    global.levelLimitMinutes = 10;
  }
  if( type == 'scratch' ){
    global.maximumLenghtToScrath = 13;
  }

  analytics().logEvent('addBonus', {
    type: type,
  });

  AsyncStorage.setItem('@BONUS', JSON.stringify(global.BONUS));
}

function checkBonuses(){
  setInterval( () => {
    var arr = global.BONUS;
    if( arr !== undefined ){

      var removed = false;

      for( let item in arr ){
        var diff = TimeDiffBetween( moment(), arr[item].expire );
        if( diff == 0 ){
          delete( global.BONUS[item] );
          if( item == 'time' ){
            global.levelLimitMinutes = 10;
          }
          if( item == 'scratch' ){
            global.maximumLenghtToScrath = 13;
          }
          var removed = true;
        }
      }

      if( removed ){
        AsyncStorage.setItem('@BONUS', JSON.stringify(global.BONUS));
      }
    }
  }, 10000);
}

export function GetLevelStatus( level, skipLocked = true ){

  if( global.TIMES[['level_' + level]] !== undefined ){
    return 'active';
  } else if( skipLocked == true && isLocked( level ) ){
    return 'lock';
  } else if( global.END[['level_' + level]] !== undefined ){
    return 'end';
  } else {
    return 'waiting';
  }

}

export function isLocked( level ){
  var TotalGuessedLogos = GetGuessedLogos();
  var NeedForUnlock = Level.Levels[ level ].minimum;
  if( TotalGuessedLogos >= NeedForUnlock ){
    return false;
  } else {
    return true;
  }

  return false;
}

export function IsGuessed( level, logo ){

  if( global.ANS[ ["level_" + level] ] === undefined ){
    var lvlArray = global.ANS[ ["level_" + level] ] = new Object();
  } else {
    var lvlArray = global.ANS[ ["level_" + level] ];
  }

  for (var key in lvlArray) {
     if(lvlArray[key].name === logo) {
       return { guessed: true, result: lvlArray[key].result };
     }
  }

  return { guessed: false, result: false };

}

export function GetLextLogo( level, key ){
  var key = key+1;
  for( let i = key; i < Level.Levels[level].logo.length; i++ ){
    if( IsGuessed(level, Level.Levels[level].logo[i].title).guessed == false ){
      return i;
    }
  }
  for( let i = 0; i < Level.Levels[level].logo.length; i++ ){
    if( IsGuessed(level, Level.Levels[level].logo[i].title).guessed == false ){
      return i;
    }
  }

  return false;
}

export function RestartLevel( level ){

  var foundAnswers = global.ANS;
  var levelVar = 'level_' + level;

  foundAnswers[levelVar] = [];

  global.ANS = foundAnswers;
  AsyncStorage.setItem('@ANS', JSON.stringify(foundAnswers));

  delete( global.END['level_' + level] );
  AsyncStorage.setItem('@END', JSON.stringify(global.END));

}

export function playEndAd(){
  var adUnitId = undefined;
  global.adRunning = true;
  if( __DEV__ ){
    var adUnitId = TestIds.INTERSTITIAL;
  } else {
    var adUnitId = 'ca-app-pub-2436333425882202/5177268588';
  }

  if( adUnitId === undefined ){
    return false;
  }

  var interstitial = InterstitialAd.createForAdRequest(adUnitId, {
      requestNonPersonalizedAdsOnly: true,
  });

  interstitial.onAdEvent((type, error, reward) => {

    global.adRunning = false;

    if( type == 'error' ){
      return false;
    } else {

      if (type === AdEventType.LOADED) {
        interstitial.show();
      }

    }

  });

  setTimeout( () => {
    global.adRunning = false;
  }, 3500 );

  interstitial.load();
}

export function EndLevel( level ){
  var endObject = global.END;

  // get summary
  var levelStatus = global.ANS[['level_' + level]];
  var countSuccess = 0;
  var countFailed = 0;

  if( levelStatus !== undefined ){
    levelStatus.map( (data, index) => {
      if( data.result == true ){
        countSuccess++;
      } else {
        countFailed++;
      }
    } );
  }

  var summary = { endTime: global.TIMES['level_' + level].end,
                  success: countSuccess,
                  fail: countFailed
                };

  if( endObject === undefined ){
    var endObject = new Object();
  }

  endObject['level_' + level ] = summary;

  global.END = endObject;
  delete( global.TIMES['level_' + level] );

  analytics().logEvent('endLevel', {
    level: level,
  });

  if( ( global.CurrentScreen == 'Stage' || global.CurrentScreen == 'Guess' ) && global.ActiveLevel == level ){
    playEndAd();
  }

  AsyncStorage.setItem('@TIMES', JSON.stringify(global.TIMES), () => {
    AsyncStorage.setItem('@END', JSON.stringify(endObject));

    if( ( global.CurrentScreen == 'Stage' || global.CurrentScreen == 'Guess' ) && global.ActiveLevel == level ){
      RootNavigation.navigate('Level', {endModal: true, levelEnded: level});
    }

  });
}

export function GetGuessedLogos(){
  var levels = Level.Levels;
  var totalGuessed = 0;
  for(var level in levels){
    var status = GetLevelStatus( level, false );

    if( status == 'end' ){

      var listing = global.END['level_' + level];
      var totalGuessed = totalGuessed + listing.success;

    }

    if( status == 'active' ){

      var listing = global.ANS['level_' + level];
      for(var logo in listing){
        if( listing[logo].result == true ){
          totalGuessed++
        }
      }

    }
  }
  return totalGuessed;
}

export function TimeDiff( time ){
  var now = moment();
  var then = moment( time );
  var diff = then.diff(now, 'seconds', false);

  if( diff < 1 ){
    return false;
  } else {
    return diff;
  }
}

export function TimeDiffBetween( from, to ){
  var now = moment( from );
  var then = moment( to );
  var diff = then.diff(now, 'seconds', false);

  if( diff > 0 ){
    return diff;
  } else {
    return 0;
  }
}


function CheckLevelTimeStatuses(){
  var CheckLevelTime = setInterval(() => {

    if( global.TIMES === undefined ){ return; }

    for ( let ind in global.TIMES ){
      var differentTime = ( TimeDiff( global.TIMES[ind].end ) );

      if( !differentTime ){
        EndLevel( parseInt(ind.replace('level_', '')) );
      }
    }
  }, 1000);
}
